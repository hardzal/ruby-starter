# Rails Command

## Creating a new rails app
``$ rails new name_app -T -api --database=postgresql``

## Starting Server
``$ rails server``

## Console in Rails like `irb`
``$ rails console``

## Creating controller
``$ rails generate controller controller_name #plural``

## Creating model
``$ rails generate model attribute1:datatype1 attribute2:datatype2 #singular``

## Show the routes
``$ rails route``

## Running migration
``$ rails db:migrate``

## Creating a change for model database
``$ rails generate migration AddAttributeToModel attribute:datatype``
``$ rails generate migration RemoveAttributeFromModel attribute:datatype``

## Command database related
``$ rails db:create``
``$ rails db:rollback``
``$ rails db:drop``
``$ rails db:reset``

## Creating Controller CRUD based
``$ rails g scaffold users username:string email:string # can use without db:create``

-----------------------------
## Gemfile command

``$ bundle install``
``$ bundle update``
``$ bundle exec rspec``
``$ bundle exec rake db:create`` 
``$ bundle exec rake db:migrate``
``$ bundle exec rails server``