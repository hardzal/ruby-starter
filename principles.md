# Programming Princples

## SOLID
 ( Single-responsibility principle,
   Open-closed principle,
   Liskov subtituion principle,
   Interface segregation principle,
   Depedency Inversion Principle
  )
## KISS (Keep It Simple Stupid)
## DRY (Don't Repeat Yourself)
## CLEAN CODE

## Composition > Inheritance
## Single Responsibility
## Separation of Concerns
## YAGNI ( You Are Gonna Need It )
## Avoid Premature Optimization
## Refactor, Refactor, Refactor
## Practices Makes Perfect
## Start At The End


### Useful links
* https://scotch.io/bar-talk/s-o-l-i-d-the-first-five-principles-of-object-oriented-design
* https://excalibur.apache.org/framework/best-practices.html
* https://dzone.com/articles/ten-commandments-of-object-oriented-design
* https://www.ibm.com/developerworks/library/os-php-7oohabits/index.html
* https://en.wikipedia.org/wiki/Category:Programming_principles
* https://www.codeproject.com/Articles/768052/Golden-Rules-Of-Good-OOP
* https://tomassetti.me/oops-concepts/
* https://stackoverflow.com/questions/1301606/how-can-i-practice-better-object-oriented-programming

https://www.youtube.com/watch?v=dTVVa2gfht8

- https://en.wikipedia.org/wiki/Software_development

- https://en.wikipedia.org/wiki/Outline_of_computer_science