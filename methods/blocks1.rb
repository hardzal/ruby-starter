def try 
  if block_given?
    yield
  else
    puts "no block"
  end
end
try

try { puts "HellO!!!" }
try do puts "Hello, blocks!" end