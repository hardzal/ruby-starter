def hello
  "Hello, World!"
end

alias hl hello

def hello
  "Hello, Ruby!"
end

puts hello
puts hl