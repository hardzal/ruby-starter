=begin
  Ruby code blocks are chunks of cde between branch or between do..end that you can associate with method invocations
=end

def call_block
  puts "Start the method"
  # you can call the block using the yield
  yield
  yield
  puts "End of the method"
end

call_block {puts "Hello!"}