def doo(*my_hobby)
  # my_hobby.inspect # ? show all value
  my_hobby.each { |h| puts "#{h}"}
end

puts doo('reading', 'writing', 'coding')
puts doo()
