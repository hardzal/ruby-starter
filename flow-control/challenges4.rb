def check_number(n)
    case 
    when n < 0
        puts "You can't enter a negative number"
    when n <= 50
        puts "#{n} is between 0 and 50"
    when n <= 100
        puts "#{n} is between 50 and 100"
    else
        puts "#{n} is above 100"
    end
end

print "Masukkan sebuah angka : "

x = gets.chomp.to_i

check_number(x)