class Greeter
  attr_accessor :names
  def initialize(names = "World")
    @names = names
  end
  def say_hi
    if @names.nil?
      puts "...?! What's your name?"
    elsif @names.respond_to?("each")
      @names.each do |name|
        puts "Hello #{name}!"
      end
    else
      puts "Hello #{@names}"
    end
  end

  def say_bye
    if @names.nil?
      puts "...? I don't know your name."
    elsif @names.respond_to?("join")
      puts "Goodbye #{names.join(", ")}. Come back soon!"
    else 
      puts "Goodbye #{names}. Come back soon!"
    end
  end
end

# ? declare a method
Greet = Greeter.new("Nama e wa")

# ? callling a method
Greet.say_hi

# ? list of method Greeter
Greeter.instance_methods

Greet.names = "Andy"

puts Greet.names

puts ""

GreetAgain = Greeter.new(["Mari", "Yuri", "Kotori"])

GreetAgain.say_hi

puts ""

GreetAgain.say_bye