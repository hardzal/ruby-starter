class MyCar
	attr_accessor :year, :color, :model_car
	
	def initialize(speed = 0, year, color, model_car)
		@speed = speed
		@year = year
		@color = color 
		@model_car = model_car
	end

	def setSpeed(s)
		@speed = s
	end

	def getSpeed
		puts "CUrrent speed is #{@speed}"
	end

	def speedUp(n)
		@speed = @speed + n
	end

	def brake(s)
		@speed = @speed - s
		puts "The car has been brake"
	end

	def shutOff
		@speed = 0
		puts "The car has been off"
	end
end


