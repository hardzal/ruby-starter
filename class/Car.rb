class Car
	attr_accessor :number_of_wheels, :seating_capacity, :maximum_velocity
	def initialize(number_of_wheels, seating_capacity, maximum_velocity)
		@number_of_wheels = number_of_wheels
		@seating_capacity = seating_capacity
		@maximum_velocity = maximum_velocity
	end
end


class ElectricCar < Car
end

tesla_car = ElectricCar.new(4, 5, 1000)
puts tesla_car.number_of_wheels
puts tesla_car.seating_capacity
puts tesla_car.maximum_velocity