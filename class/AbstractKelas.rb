class AbstractKlass
  def welcome
    puts "#{hello} #{name}"
  end
end

class ConcacreteClass < AbstractKlass
  def hello; "Hello"; end
  def name; "Ruby students"; end
end

ConcacreteClass.new.welcome