class Lingkaran
  attr_accessor :jarijari
  # ? public
  # ? protected
  # ? private
  def initialize(jarijari)
    @jarijari = jarijari
  end

  def luas_lingkaran
    @jarijari*@jarijari*(22/7)
  end

  def keliling_lingkaran
    @jarijari*2*(22/7)
  end
end

lingkaran = Lingkaran.new(14)

puts "Luas lingkaran = " + lingkaran.luas_lingkaran.to_s
puts "Keliling lingkaran = " + lingkaran.keliling_lingkaran.to_s
