class Mahasiswa
  # ? Getter and setter otomatis
  attr_accessor :nama, :nim, :jenis_kelamin

  def initialize(nama, nim, jenis_kelamin)
    @nama = nama
    @nim = nim
    @jenis_kelamin = jenis_kelamin
  end
end

mahasiswa = Mahasiswa.new("M Rizal", 123170036, "L")

puts mahasiswa.nama

mhs = Mahasiswa.new()

mhs.nama = "siapa anda";

puts mhs.nama
  