class Vehicle
    # ? public attribute
    # implements the getter method
    # attr_reader :number_of_wheels

    # implements the setter method 
    # attr_writer :number_of_wheels

    # implements both methods
    attr_accessor :number_of_wheels

	def initialize(number_of_wheels, type_of_tank, seating_capacity, maximum_velocity)
		      @number_of_wheels = number_of_wheels
		      @type_of_tank = type_of_tank
		      @seating_capacity = seating_capacity
		      @maximum_velocity = maximum_velocity
    end
    
    def number_of_wheels
        @number_of_wheels
    end

    def set_of_number_wheels=(number)
        @number_of_wheels = number
    end

    def make_noise
        "VRUUUUMM"
    end
        
end

tesla_model_s = Vehicle.new(4, 'electric', 5, 250)
print tesla_model_s.number_of_wheels

print tesla_model_s.make_noise