module Luas
    attr_accessor :sisi, :panjang, :lebar, :tinggi, :alas, :Luas
end

class Persegi
    include Luas

    def initialize(sisi)
        @sisi = sisi
    end

    def set_luas
        @luas = @sisi*@sisi
    end

    def get_luas
        return @luas
    end
end

print "Input Sisi : " 
n = gets.chomp.to_i
persegi = Persegi.new(n)

persegi.set_luas
puts "Luas : #{persegi.sisi} * #{persegi.sisi} = #{persegi.get_luas}"