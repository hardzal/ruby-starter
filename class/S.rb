class S 
  puts "Just started class S"
  puts self # the current or default object accessible to you 
  
  module M
    puts "Nested module S::M"
    puts self
  end

  puts "Back in the outer level of S"
  puts self
end