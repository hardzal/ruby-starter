# declare a variable
nama = "Sakti mandraguna"

# Print output
puts "Hello, #{nama}"

# input data
# 'gets' input with newline
# 'gets.chomp' input without newline

# Array
angka = [3, 4, 5, 6, 7]

# Hash
kelas = {:backend => ['Pascales', 'Ervien']}

# Output the array
for i in angka
    print "#{i}"
end

puts 

kelas.each do |key, value|
    print "#{key} => #{value}"
end

puts 

if 1 > 2
    puts "1 is greeter than 2"
elsif 2 > 1
    puts "2 is greeter than 1"
else
    puts "1 is equal to 2"
end

bookshelf = []
bookshelf << "Learn Ruby"
bookshelf << "Learn Rails"
print bookshelf[0] + ""


