# Ruby-Starter
### Binar Academy (Back End Developer - Ruby On Rails)

## Pertemuan 1
- install ruby (2.5.1)
- install rbenv/rvm 
- install postgresql
- install rails 

    `rails new blog --api -T --database=postgresql`
- basic ruby

## Pertemuan 2
- [starting gitlab](git-cheatsheet.md) (version controlling)

## Pertemuan 3
- [Basic Ruby](ruby-cheatsheet.rb)

## Pertemuan 4
- Basic Object Oriented Programming
- [Basic Rails](rails-cheatsheet.md)

## Pertemuan 5
- Continue learning Rails
- < Princples >
  - S.O.L.I.D
- < Unit Testing > 
    - TDD [[Test Driven Development](https://en.wikipedia.org/wiki/Test-driven_development)]
    - BDD [[Behaviour Driven Development](https://en.wikipedia.org/wiki/Behavior-driven_development)]
  
## Pertemuan 6
- Refactor code
- Mengenai gemfile

Contoh: 
- Active Model Serializer

Step to install gemfile
1. Put this code in gemfile
```ruby
 gem 'active_model_serialization'
```
2. Run this code in terminal
```terminal
 bundle install
```

## Party Class (8 Oktober 2018)
- < Development Methods >
    - Agile->Scrum
    - Waterfall

## Pertemuan 7

#### Heroku CI/CD

``
$ heroku login
``

``
$ heroku run
``

- Push repository to gitlab

``
$ heroku run rails db migrate -a name-app
``
``
$ heroku run rails db:migrate -a name-app
``

- Checking gitlab repository in pipelines

``
$ heroku logs -a name-app
``

``
$ heroku logs -t -a name-app
``

## Pertemuan 8
Active Records
ORM = Object Relational Mapping 

### CRUD
#### Create
`` $ user = User.create(name: "David", email: "email@email.com")``
#### Read
``$ User.all``
#### Update
``$ user = User.find_by(id: 1)``
``$ user.update(name: 'Nama baru')``
### Delete
``$ user = User.find_by(id: 1)``
``$ user.destroy``

### Rails Console
### Active Records Validations
### Active Records Association 
### Active Records Migrations

``$ rails g model ModelName name:type_name``

``$ rails g controller ModelNames``

``$ rails g migration AddUserToArticle user:references``

``$ rails db:migrate``

## Pertemuan 9
### Design API
 - What is API
 - RESTFulAPI
 - URL Design
 - Endpoint

### HTTP Verbs
 - GET
 - POST
 - PUT
 - DELETE

### HTTP Response Code Like (1xx, 2xx, 3xx, 4xx, 5xx)
   website reference: httpstatusdogs, httpstatuscat
### SOAP
### GRPC
### RESTApi
### GraphQL->reference: howtographql

rails latihan-auth --api -T --database=postgresql
gem 'bcrypt', '~> 3.1.7'
gem 'knock'
gem 'jwt'
gem 'active_model_serializers'
bundle install
rails g model user username:string email:string password_digest:string
rails g controller users
rails g model article title:string body:text

rails g knock:install

rails generate knock:install

rails generate knock:token_controller user 

rails db:create

rails db:migrate

# create routes.rb
# modify user_controller
# running rails server
# how to sign up
{
	"user": {
	}
}

# modify config/secret.yml then run
rails secret

# copy the output 
development:
secret_key_base:
 secret_code

# modify config/initializers/knock.rb
 config.token_secret_signature_key = -> { Rails.application.credentials.read }

# modify user_toke_controller.rb
 skip_before_action :verify_authenticity_token

# login to POST->localhost:3000/users/signin
{

}

# then output jwt code

# move to headers
 key: Authorization
 value: jwt_code
 key: content-type
 value: application/json


#### REFERENCE LINKS

- http://railscasts.com/ 

- https://github.com/rubocop-hq/ruby-style-guide

- https://www.ruby-lang.org/en/documentation/

- https://www.techotopia.com/index.php/Ruby_Essentials

- http://ruby-doc.com/docs/ProgrammingRuby/

- http://ruby-doc.org/

- https://docs.ruby-lang.org/en/

- http://ruby-for-beginners.rubymonstas.org

- https://gorails.com/setup/ubuntu/18.04

- https://www.digitalocean.com/community/tutorials/how-to-install-and-use-postgresql-on-ubuntu-16-04

- https://rizkimufrizal.github.io/belajar-ruby-on-rails/

- https://medium.com/otavio-henrique/creating-simple-rest-api-with-rails-5-1ba71b37cad

- https://github.com/markets/awesome-ruby

- https://guides.rubyonrails.org/getting_started.html

- https://www.driftingruby.com/episodes

- http://www.belajarrubyonrails.com/p/daftar-isi.html

- https://www.pusher.com/tutorials/live-blog-ruby-rails/


- http://guides.railsgirls.com/app

- https://medium.com/@bruno_boehm/full-blog-app-tutorial-on-rails-zero-to-deploy-4c19e8174791
  
  
- https://medium.freecodecamp.org/lets-create-an-intermediate-level-ruby-on-rails-application-d7c6e997c63f

- 
# Community

- https://id-ruby.slack.com

-------------------------

# Saved Links

- https://www.mapbox.com/account/